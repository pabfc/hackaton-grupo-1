import {LitElement, html} from 'lit-element';
import '../seguros-listado/seguros-listado.js';

class SegurosMain extends LitElement {

    static get properties() {
        return {
            seguros: {type: Array}            
        }; 
    }

    constructor () {
        super();
        this.seguros = [];
        this.getInsuranceData();
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="row" id="segurosList">
            <div class="row row-cols-1 row-cols-sm-4">
            ${this.seguros.map(
                seguros => html`
                <seguros-listado
                    name="${seguros.name}" 
                    desc="${seguros.desc}"
                    photo="${seguros.rute}"
                    alt="${seguros.rute_alt}"
                    price="${seguros.price}"
                    id="${seguros.id}"
                    @order-insurance=${this.orderInsurance}
                    @order-refresh=${this.orderRefresh}
                >
                </seguros-listado>`
            )}
            </div>
        `;
    }    
    
    getInsuranceData() {
        console.log("Accedo a getInsuranceData");
        console.log("Obteniendo datos de los seguros a la venta");

        let xhr = new XMLHttpRequest();   //estándar para traerse datos del servidor (llamar a una API)
        //si ponemos xhr = new XMLHttpRequest(), se declará a nivel global
        
        xhr.onload = () => {
            if (xhr.status === 200) {   //el estado de la petición XMLHttpRequest está en status
                console.log("He lanzado la petición y el servidor me ha contestado con un " + xhr.status + ", es decir, la petición se ha completado correctamente");

                let APIResponse = JSON.parse(xhr.responseText);    //la respuesta a la llamada al XMLHttpRequest estará en responseText; es un string, un texto
                console.log(APIResponse);
                //¿cómo preparo ese string (un json) para entresacar la info? Con el parse de JSON.
                //console.log("xhr.responseText:" + xhr.responseText);    //con esto pinto todo el JSON

                this.seguros = APIResponse;   //results es cómo está organizado el json que me devuelve esta API
            }
        };

        xhr.open("GET", "http://localhost:8081/hackaton/v2/insurances/");   //aquí ponemos la URL donde esté el backend disponible, el que nos digan ellos
        xhr.send();   //esta línea es en donde se hace la petición
        //en el Mozilla Development etc... está cómo mandar un POST
        console.log(this.seguros);
        console.log("Finaliza getInsuranceData"); 
    }

    orderInsurance (e)  {
        console.log("Accedo a orderInsurance");
        console.log("Vamos a mandar el seguro que queremos contratar");

        console.log(e);
        console.log(e.detail.id_cliente);
        console.log(e.detail.id);
        console.log(e.detail.name);
        console.log(e.detail.desc);
        console.log(e.detail.price);

        var data = {};
        /*
        data.id_cliente = e.detail.id_cliente;
        data.id_producto = e.detail.id;*/

        data.userId = e.detail.id_cliente;
        data.insuranceId = e.detail.id;
        var json = JSON.stringify(data);

        console.log("Se va a envia un POST de nueva contratacion");
        console.log(json);

        let xhr = new XMLHttpRequest();   //estándar para traerse datos del servidor (llamar a una API)
        //si ponemos xhr = new XMLHttpRequest(), se declará a nivel global
        //xhr.open("POST", "http://localhost:8081/hackaton/v2/insurances/", true);   //aquí ponemos la URL donde esté el backend disponible, el que nos digan ellos
        xhr.open("POST", "http://localhost:8081/hackaton/v2/purchases/", true);   //aquí ponemos la URL donde esté el backend disponible, el que nos digan ellos
        xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        
        xhr.onload = function () {
            //console.log(xhr.status);
            if (xhr.status === 200) {
                console.log("Ejecución correcta del POST");
                console.log(this);                
            } else {
                console.log("Ejecución errónea");
            }
        }

        xhr.send(json);
        this.dispatchEvent(
            new CustomEvent(
                "order-refresh",
                {
                    "detail" : {
                        "id_cliente": e.detail.id_cliente,
                        "id": e.detail.id,
                        "name": e.detail.name,
                        "desc": e.detail.desc,
                        "price": e.detail.price,
                    }
                }
            )
        );


        console.log("Finaliza orderInsurance");
    }

}



customElements.define('seguros-main', SegurosMain) 