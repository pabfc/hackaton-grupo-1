import {LitElement, html} from 'lit-element';
import '../seguros-main/seguros-main.js';
import '../seguros-sidebar/seguros-sidebar.js';

class SegurosApp extends LitElement {

    static get properties() {
        return {
            seguros: {type: Array}            
        }; 
    }

    constructor () {
        super();
        this.seguros = [];
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="d-flex justify-content-center">
                <h1 style="font-family:georgia,garamond,serif;">Katakroker Seguros</h1>
            </div>
            <br/>
            <hr></hr>
            <br/>
            <div class="row">
                <seguros-main @order-refresh="${this.orderRefresh}" class="col-9"></seguros-main>
                <seguros-sidebar @enable-order="${this.enableOrder}" class="col-3" id="sidebar"></seguros-sidebar>
            </div>
        `;
    }    
    
    orderRefresh ()  {
        console.log("orderRefresh");
        this.shadowRoot.querySelector("seguros-sidebar").refreshOrder = true;

    }

    enableOrder(e) {
        console.log("enableOrder");
        console.log(e.detail.id);
        this.shadowRoot.querySelector("seguros-main").enableOrder = e.detail.id;
    }
}



customElements.define('seguros-app', SegurosApp) 