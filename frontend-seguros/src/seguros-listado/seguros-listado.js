import {LitElement, html} from 'lit-element';

class SegurosListado extends LitElement {

    static get properties() {
        return {
            id: {type: String},
            name: {type: String},
            desc: {type: String},
            price: {type: String},
            photo: {type: String},
            alt: {type: String}
        };
    }

    constructor () {
        super();
    }
    //<div class="card-custom-img" style="background-image: url(img/travel.png);"></div>
    //<div class="card-custom-img" style="background-image: url(http://res.cloudinary.com/d3/image/upload/c_scale,q_auto:good,w_1110/trianglify-v1-cs85g_cc5d2i.jpg);"></div>
    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <link href="../../css/cards.css" rel="stylesheet">            
            <div class="card card-custom bg-white border-white border-0">
                <div class="card-custom-img" style="background-image: url(${this.photo});"></div>
                    <div class="card-custom-avatar">
                        <img class="img-fluid" src="./img/kata.png" alt="${this.alt}" />
                    </div>
                    <div class="card-body" style="overflow-y: auto">
                        <h4 class="card-title">${this.name}</h4>
                        <p class="card-text">${this.desc}</p>
                        <p class="card-text">${this.price} €</p>
                    </div>
                    <div class="card-footer" style="background: inherit; border-color: inherit;">
                        <button id="contratar-${this.id}"  @click="${this.contratar}" class="btn btn-primary">Contratar</button>
                        <button class="btn btn-outline-primary">Más información</button>
                    </div>
            </div>
          </div>
        `;
    }
    
    contratar ()  {
        console.log("Accedo a contratar");
        console.log("Vamos a mandar a seguros-main el seguro que queremos contratar");

        this.dispatchEvent(
            new CustomEvent(
                "order-insurance",
                {
                    "detail" : {
                        "id_cliente": "1",
                        "id": this.id,
                        "name": this.name,
                        "desc": this.desc,
                        "price": this.price,
                    }
                }
            )
        );

        this.shadowRoot.getElementById("contratar-"+this.id).disabled = true;

        console.log("Finaliza contratar seguros-listados");
    }
    
}

customElements.define('seguros-listado', SegurosListado) 