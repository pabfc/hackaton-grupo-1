import { LitElement, html } from 'lit-element';

class SegurosSidebar extends LitElement {

    static get properties() {
        return {
            segurosContratados: {type: Array},
            refreshOrder: {type: Boolean}          
        }; 
    }

    constructor () {
        super();
        this.segurosContratados = [];
        this.getInsuranceOrdered();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <aside class="h-100" style="background-color: lightgrey; padding: 5%;">
                <section>
                    <h3>Seguros contratados</h3>
                    <hr></hr>
                    <ul class="list-group">
                        ${this.segurosContratados.map(
                            seguro => html`<li class="list-group-item">${seguro.name}: ${seguro.price} €
                                <button class="w-30 btn btn-danger" id="cancel-${seguro.id}" @click="${this.cancel}">Anular</button>
                            </li>`
                        )}
                        
                    </ul>
                    <div class="mt-5">
                        
                    </div>
                </section>
            </aside>
        `;
    }

    updated(changedProperties) {
        console.log("updated en sidebar");

        if (changedProperties.has("refreshOrder")) {
            console.log("Ha cambiado el valor de la propiedad refreshOrder");
            console.log(this.refreshOrder);
            this.getInsuranceOrdered();
        }


    }

    getInsuranceOrdered() {
        console.log("Accedo a getInsuranceOrdered");
        console.log("Obteniendo datos de la lista de seguros");

        let xhr = new XMLHttpRequest();
        
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("He lanzado la petición y el servidor me ha contestado con un " + xhr.status + ", es decir, la petición se ha completado correctamente");

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                this.segurosContratados = APIResponse;
            }
        };

        xhr.open("GET", "http://localhost:8081/hackaton/v2/purchases/");   //aquí ponemos la URL donde esté el backend disponible, el que nos digan ellos
        //xhr.open("GET", "http://localhost:8081/hackaton/v2/insurances/");   //aquí ponemos la URL donde esté el backend disponible, el que nos digan ellos
        xhr.send();   //esta línea es en donde se hace la petición
        //en el Mozilla Development etc... está cómo mandar un POST
        console.log(this.segurosContratados);
        console.log("Finaliza getInsuranceOrdered"); 
    }

    confirm() {
        console.log("Entro en confirm en persona-sidebar");
        console.log("Se va a confirmar la lista de seguros");

        //this.dispatchEvent(new CustomEvent("confirm", {}));
    }

    cancel (e) {
        e.preventDefault();
        console.log("Entramos en cancel");
        var target = e.target;
        var id = target.id;
        console.log(e.target);
        /*console.log("Pintamos el evento");
        console.log(e);
        console.log("Pintamos el target");
        console.log(e.target);*/
        console.log("El id que vamos a cancelar");
        console.log(id);
        var id_prod = id.split("-")[1];
        console.log("id_producto " + id_prod);


        //POST para hacer la baja
        this.sendCancelOrder(id_prod);
        //llamar a getInsuranceOrder para refrescar
        //this.getInsuranceOrdered();

        //rehabilitar botón contratar
        
        this.dispatchEvent(
            new CustomEvent(
                "enable-order",
                {
                    "detail" : {                        
                        "id": e.detail.id,
                        "name": e.detail.name
                    }
                }
            )
        );

    }

    sendCancelOrder(id_prod) {

        console.log("Se va a enviar una cancelacion del producto " );
        
        console.log(this.segurosContratados);
        console.log(id_prod);

        let xhr = new XMLHttpRequest();   //estándar para traerse datos del servidor (llamar a una API)
        //si ponemos xhr = new XMLHttpRequest(), se declará a nivel global
        //xhr.open("POST", "http://localhost:8081/hackaton/v2/insurances/", true);   //aquí ponemos la URL donde esté el backend disponible, el que nos digan ellos
        xhr.open("DELETE", "http://localhost:8081/hackaton/v2/purchases/" + id_prod, true);   //aquí ponemos la URL donde esté el backend disponible, el que nos digan ellos
        xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        
        xhr.onload = function () {            
            console.log("Estado del cancel: "+ xhr.status);
            console.log("Mira luego en BBDD");
            //this.getInsuranceOrdered();
            /*
            if (xhr.readyState == 4 && xhr.status == "201") {
                console.log("Ejecución correcta del CANCEL");
            } else {
                console.log("Ejecución errónea");
            }*/
        }

        xhr.send();
    }


}

customElements.define('seguros-sidebar', SegurosSidebar)