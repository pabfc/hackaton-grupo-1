package com.techu.apitechuv2.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import java.util.Map;

@Document (collection = "purchases")
public class PurchaseModel {
    @Id
    private String id;
    private String insuranceId;
    private String userId;
    private float amount;
    private Map<String, Integer> purchaseItems;
    private String name;
    private String desc;
    private String rute;
    private float price;
    private String rute_alt;
    private String info_add;



    public PurchaseModel() {
    }

    public PurchaseModel(String id, String insuranceId, String userId, float amount, Map<String, Integer> purchaseItems, String name, String desc, String rute, float price, String rute_alt, String info_add) {
       this.id = id;
        this.insuranceId = insuranceId;
        this.userId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
        this.name = name;
        this.desc = desc;
        this.rute = rute;
        this.price = price;
        this.rute_alt = rute_alt;
        this.info_add = info_add;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(String insuranceId) {
        this.insuranceId = insuranceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRute() {
        return rute;
    }

    public void setRute(String rute) {
        this.rute = rute;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getRute_alt() {
        return rute_alt;
    }

    public void setRute_alt(String rute_alt) {
        this.rute_alt = rute_alt;
    }

    public String getInfo_add() {
        return info_add;
    }

    public void setInfo_add(String info_add) {
        this.info_add = info_add;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Map<String, Integer> getPurchaseItems() {
        return purchaseItems;
    }

    public void setPurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}




