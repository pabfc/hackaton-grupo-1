package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.InsuranceModel;
import com.techu.apitechuv2.models.PurchaseModel;
import com.techu.apitechuv2.repositories.PurchaseRepository;
import com.techu.apitechuv2.services.InsuranceService;
import com.techu.apitechuv2.services.PurchaseService;
import com.techu.apitechuv2.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/hackaton/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET , RequestMethod.POST, RequestMethod.DELETE})
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    InsuranceService insuranceService;

    @PostMapping("/purchases")
   /* public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("La id del usuario de la com npra es " + purchase.getUserId());
        System.out.println("Los id del seguro es " + purchase.getInsuranceId());
        Optional<InsuranceModel> insurance =  this.insuranceService.findById(purchase.getInsuranceId());
        purchase.setName(insurance.get().getName());
        purchase.setDesc(insurance.get().getDesc());
        purchase.setInsuranceId(insurance.get().getId());
        purchase.setAmount(insurance.get().getPrice());
        purchase.setRute(insurance.get().getRute());
        purchase.setRute_alt(insurance.get().getRute_alt());
        purchase.setInfo_add(insurance.get().getInfo_add());

        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(result, result.getHttpStatus());
    }*/
public List<PurchaseModel> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("La id del usuario de la com npra es " + purchase.getUserId());
        System.out.println("Los id del seguro es " + purchase.getInsuranceId());
        System.out.println("Los id de la compra es  " + purchase.getUserId() + purchase.getInsuranceId());
        Optional<InsuranceModel> insurance =  this.insuranceService.findById(purchase.getInsuranceId());
        purchase.setName(insurance.get().getName());
        purchase.setDesc(insurance.get().getDesc());
        purchase.setInsuranceId(insurance.get().getId());
        purchase.setPrice(insurance.get().getPrice());
        purchase.setRute(insurance.get().getRute());
        purchase.setRute_alt(insurance.get().getRute_alt());
        purchase.setInfo_add(insurance.get().getInfo_add());
        purchase.setId( purchase.getUserId() + purchase.getInsuranceId());

        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchase);

        /*return new ResponseEntity<>(result, result.getHttpStatus());*/
        return this.purchaseService.findAll();
        }

    @DeleteMapping("/purchases/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id ) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a eliminar " + id);
        boolean deleteProduct = this.purchaseService.deletePurchase(id);
        return new ResponseEntity<>(
                deleteProduct?"Producto Borrado" : "Producto no encontrado",
                deleteProduct? HttpStatus.OK: HttpStatus.NOT_FOUND
        );

    }

    @GetMapping("/purchases")
    public List<PurchaseModel> getPurchases() {
        System.out.println("getPurchases");

        return this.purchaseService.findAll();

    }


}
