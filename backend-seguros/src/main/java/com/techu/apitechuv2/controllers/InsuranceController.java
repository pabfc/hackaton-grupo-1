package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.InsuranceModel;
import com.techu.apitechuv2.services.InsuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/hackaton/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET , RequestMethod.POST})
public class InsuranceController {

    @Autowired
    InsuranceService insuranceService;

    @GetMapping("/insurances")
    public List<InsuranceModel> getInsurances() {
        System.out.println("getInsurances");

        return this.insuranceService.findAll();

    }

    @GetMapping("/insurances/{id}")
    public ResponseEntity<Object> getInsuranceById(@PathVariable String id) {
        System.out.println("getInsuranceById");
        System.out.println("La id del seguro a buscar es " + id);

        Optional<InsuranceModel> result = this.insuranceService.findById(id);

        if (result.isPresent() == true) {
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Seguro no encontrado", HttpStatus.NOT_FOUND);

        }

    }

    @PostMapping("/insurances")
    public ResponseEntity<InsuranceModel> addInsurance(@RequestBody InsuranceModel insurance) {
        System.out.println("addInsurance");
        System.out.println("La id del seguro que se va a crear es " + insurance.getId());
        System.out.println("El nombre del seguro que se va a crear es " + insurance.getName());
        System.out.println("La descripción del seguro que se va a crear es " + insurance.getDesc());
        System.out.println("El precio del seguro que se va a crear es " + insurance.getPrice());
        System.out.println("La ruta del seguro que se va crear es " + insurance.getRute());

        return new ResponseEntity<>(this.insuranceService.add(insurance), HttpStatus.CREATED);
    }

    @PutMapping("/insurances/{id}")
    public ResponseEntity<InsuranceModel> updateInsurance(@RequestBody InsuranceModel insurance, @PathVariable String id) {
        System.out.println("updateInsurance");
        System.out.println("La id del seguro a modificar es " + id);
        System.out.println("El nombre el seguro a actualizar" + insurance.getName());
        System.out.println("La descripcion del seguro a actualizar" + insurance.getDesc());
        System.out.println("El precio del seguro a actualizar" + insurance.getPrice());

        Optional<InsuranceModel> insuranceToUpdate =this.insuranceService.findById(id);

        if (insuranceToUpdate.isPresent() == true) {
            System.out.println("Seguro para actualizar encontraado, actualizando");
            this.insuranceService.update(insurance);
        }

        return new ResponseEntity<>(insurance,
                insuranceToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }

    @DeleteMapping ("/insurances/{id}")
    public ResponseEntity<String> deleteInsurance(@PathVariable String id)  {
        System.out.println("deleteInsurance");
        System.out.println("La ide del seguro a borrar es" + id);

        boolean deteleInsurance = this.insuranceService.delete(id);

        return new ResponseEntity<>(
                deteleInsurance ? "seguro borrado" : "seguro no borrado",
                deteleInsurance ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}